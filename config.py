database = {
    'driver': "postgresql+asyncpg",
    'host': "localhost",
    'port': 5432,
    'database': "postgres",
    'user': "postgres",
    'password': "password"
}

server = {
    'login_service_host': 'localhost',
    'login_service_port': 8082,
}