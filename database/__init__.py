import os

from database.db_connection import make_db_url, AsyncDBConnection

url = os.environ['DB_URL'] if 'DB_URL' in os.environ else make_db_url()
db_connection = AsyncDBConnection(url)
