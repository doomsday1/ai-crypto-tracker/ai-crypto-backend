from fastapi import APIRouter

from .roles import db_roles_router
from .users import db_users_router

db_router = APIRouter()

db_router.include_router(
    db_users_router,
    prefix="/users",
    tags=["Users"]
)

db_router.include_router(
    db_roles_router,
    prefix="/roles",
    tags=["Roles"]
)
