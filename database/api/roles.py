from typing import List

from fastapi import APIRouter, HTTPException, status, Depends

from database import db_connection as db
from database.repositories.roles import RoleRepository
from database.schemas.roles import RoleFull, RoleBase

db_roles_router = APIRouter()


@db_roles_router.get("/list", response_model=List[RoleFull])
async def list_roles(
        role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    roles = await role_repo.list_all()
    if roles is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return roles


@db_roles_router.get("/{role_id}", response_model=RoleFull)
async def get_roles(
        role_id: int,
        role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    role = await role_repo.get(role_id)
    if role is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return role


@db_roles_router.post("", response_model=RoleFull)
async def create_role(
    role: RoleBase,
    role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    new_role = await role_repo.create(role)
    if new_role is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )
    return new_role


@db_roles_router.patch("/{role_id}", response_model=RoleFull)
async def update_role(
    role_id: int,
    role: RoleBase,
    role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    updated_role = await role_repo.update(role_id, role)
    if updated_role is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )

    return updated_role


@db_roles_router.delete("/{role_id}", response_model=RoleFull)
async def delete_role(
        role_id: int,
        role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    role = await role_repo.delete(role_id)
    if role is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )
    return role
