from typing import List

from fastapi import APIRouter, HTTPException, status, Depends

from database import db_connection as db
from database.repositories.roles import RoleRepository
from database.repositories.users import UserRepository
from database.schemas.roles import RoleFull
from database.schemas.users import UserCreate, UserOut, UserUpdate

db_users_router = APIRouter()


@db_users_router.get("/list", response_model=List[UserOut])
async def list_users(
        user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    users = await user_repo.list_all()
    if users is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return users


@db_users_router.get("/{user_id}", response_model=UserOut)
async def get_user(
        user_id: int,
        user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    user = await user_repo.get(user_id)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return user


@db_users_router.post("", response_model=UserOut)
async def create_user(
    user: UserCreate,
    user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    new_user = await user_repo.create(user)
    if new_user is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )
    return new_user


@db_users_router.patch("/{user_id}", response_model=UserOut)
async def update_user(
    user_id: int,
    user: UserUpdate,
    user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    updated_user = await user_repo.update(user_id, user)
    if updated_user is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )

    return updated_user


@db_users_router.delete("/{user_id}", response_model=UserOut)
async def delete_user(
        user_id: int,
        user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    user = await user_repo.delete(user_id)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_304_NOT_MODIFIED
        )
    return user


@db_users_router.get("/{user_id}/roles", response_model=List[RoleFull], tags=["Roles"])
async def list_user_roles(
        user_id: int,
        user_repo: UserRepository = Depends(db.get_repository(UserRepository))
):
    user = await user_repo.get(user_id)
    roles = await user_repo.get_user_roles(user)
    if roles is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return roles


@db_users_router.post("/{user_id}/roles", response_model=RoleFull, tags=["Roles"])
async def add_user_role(
        user_id: int,
        role_id: int,
        user_repo: UserRepository = Depends(db.get_repository(UserRepository)),
        role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    user = await user_repo.get(user_id)
    role = await role_repo.get(role_id)
    await user_repo.add_user_role(user, role)
    return role


@db_users_router.delete("/{user_id}/roles", response_model=RoleFull, tags=["Roles"])
async def delete_user_role(
        user_id: int,
        role_id: int,
        user_repo: UserRepository = Depends(db.get_repository(UserRepository)),
        role_repo: RoleRepository = Depends(db.get_repository(RoleRepository))
):
    user = await user_repo.get(user_id)
    role = await role_repo.get(role_id)
    await user_repo.delete_user_role(user, role)
    return role
