from typing import Any, Type, Generator

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

import config
from database.models.base import Base
from database.repositories.base import Repository


class AsyncDBConnection:
    def __init__(self, url: str):
        self._engine = create_async_engine(url, echo=False)
        # noinspection PyTypeChecker
        self._session_maker = \
            sessionmaker(
                self._engine,
                expire_on_commit=False,
                class_=AsyncSession
            )

    async def create_models(self):
        async with self._engine.begin() as connection:
            await connection.run_sync(Base.metadata.create_all)

    async def _get_db(self) -> Generator:
        db = None
        try:
            db = self._session_maker()
            yield db
        finally:
            if isinstance(db, AsyncSession):
                await db.close()

    def get_repository(
            self,
            repository: Type[Repository],
    ) -> Any:
        def repository_getter(db: AsyncSession = Depends(self._get_db)) -> Repository:
            return repository(db)

        return repository_getter


def make_db_url() -> str:
    db = config.database
    return (f"{db['driver']}://"
            f"{db['user']}:{db['password']}@"
            f"{db['host']}:{db['port']}/{db['database']}")
