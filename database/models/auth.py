from sqlalchemy import Column, Integer, String, ForeignKey, Index, Boolean
from .base import Base


class User(Base):
    __tablename__ = 'users'

    id: Column = Column(Integer, primary_key=True)  # Will make SERIAL
    username: Column = Column(String, unique=True, nullable=False)
    full_name: Column = Column(String, nullable=False)
    email: Column = Column(String, unique=True, nullable=False)
    hashed_password: Column = Column(String, nullable=False)
    disabled: Column = Column(Boolean, nullable=False)

    __table_args__ = (
        Index('users_name_index', 'username', unique=True),
        Index('users_email_index', 'email', unique=True),
    )


class Role(Base):
    __tablename__ = 'roles'

    id: Column = Column(Integer, primary_key=True)  # Will make SERIAL
    role: Column = Column(String, unique=True, nullable=False)


class UserRole(Base):
    __tablename__ = 'user_roles'

    user_id: Column = Column(Integer, ForeignKey('users.id'), primary_key=True)
    role_id: Column = Column(Integer, ForeignKey('roles.id'), primary_key=True)

    __table_args__ = (
        Index('user_roles_index', 'user_id', 'role_id', unique=True),
    )
