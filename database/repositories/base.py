from typing import List

from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession


class Repository:
    def __init__(self, db_session: AsyncSession):
        self._session = db_session

    # noinspection PyMethodMayBeStatic
    async def list_all(self) -> List[BaseModel]:
        return [BaseModel()]

    # noinspection PyMethodMayBeStatic
    async def get(self, entity_id: int) -> BaseModel:
        return BaseModel()

    # noinspection PyMethodMayBeStatic
    async def create(self, entity: BaseModel) -> BaseModel:
        return entity

    # noinspection PyMethodMayBeStatic
    async def update(self, entity_id: int, entity: BaseModel) -> BaseModel:
        return entity

    # noinspection PyMethodMayBeStatic
    async def delete(self, entity_id: int) -> BaseModel:
        return BaseModel()
