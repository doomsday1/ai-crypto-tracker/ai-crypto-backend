from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from database.models.auth import Role
from database.repositories.base import Repository
from database.schemas.roles import RoleBase


# noinspection DuplicatedCode
class RoleRepository(Repository):
    def __init__(self, db_session: AsyncSession):
        super().__init__(db_session)

    async def list_all(self) -> List[Role]:
        query = select(Role)
        role_db_obj = await self._session.execute(query)
        return [role for role, in role_db_obj]

    async def get(self, role_id: int) -> Role | None:
        role_db_obj = await self._session.get(Role, role_id)
        return role_db_obj

    async def create(self, role: RoleBase) -> Role:
        role_db_obj = Role(**role.model_dump(exclude_unset=True))
        self._session.add(role_db_obj)
        await self._session.commit()
        await self._session.refresh(role_db_obj)
        return role_db_obj

    async def update(self, role_id: int, role: RoleBase) -> Role | None:
        role_db_obj = await self._session.get(Role, role_id)
        for key, value in role.model_dump(exclude_unset=True).items():
            setattr(role_db_obj, key, value)

        await self._session.commit()
        await self._session.refresh(role_db_obj)
        return role_db_obj

    async def delete(self, role_id: int) -> Role | None:
        role_db_obj = await self._session.get(Role, role_id)
        await self._session.delete(role_db_obj)
        await self._session.commit()
        return role_db_obj
