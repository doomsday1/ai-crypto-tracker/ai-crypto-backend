import bcrypt

from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from database.models.auth import User, Role, UserRole
from database.repositories.base import Repository
from database.schemas.users import UserCreate, UserUpdate


# noinspection DuplicatedCode
class UserRepository(Repository):
    def __init__(self, db_session: AsyncSession):
        super().__init__(db_session)

    async def list_all(self) -> List[User]:
        query = select(User)
        user_db_obj = await self._session.execute(query)
        return [user for user, in user_db_obj]

    async def get(self, user_id: int) -> User | None:
        user_db_obj = await self._session.get(User, user_id)
        return user_db_obj

    async def create(self, user: UserCreate) -> User:
        hashed_password = bcrypt.hashpw(user.password.encode("utf-8"),
                                        bcrypt.gensalt(12, prefix=b"2b"))
        user_db_obj = User(**user.model_dump(exclude={'password'}, exclude_unset=True),
                           hashed_password=hashed_password.decode("utf-8"))
        self._session.add(user_db_obj)
        await self._session.commit()
        await self._session.refresh(user_db_obj)
        return user_db_obj

    async def update(self, user_id: int, user: UserUpdate) -> User | None:
        user_db_obj = await self._session.get(User, user_id)
        for key, value in user.model_dump(exclude={'password'}, exclude_unset=True).items():
            setattr(user_db_obj, key, value)

        if 'password' in user:
            user_db_obj.hashed_password = \
                bcrypt.hashpw(user.password.encode("utf-8"),
                              bcrypt.gensalt(12, prefix=b"2b"))

        await self._session.commit()
        await self._session.refresh(user_db_obj)
        return user_db_obj

    async def delete(self, user_id: int) -> User | None:
        user_db_obj = await self._session.get(User, user_id)
        await self._session.delete(user_db_obj)
        await self._session.commit()
        return user_db_obj

    async def get_user_by_username(self, username: str) -> User:
        query = select(User).filter(User.username.ilike(username))
        user_db_obj = await self._session.execute(query)
        return user_db_obj.scalar_one_or_none()

    async def get_user_by_email(self, email: str) -> User:
        query = select(User).filter(User.email.ilike(email))
        user_db_obj = await self._session.execute(query)
        return user_db_obj.scalar_one_or_none()

    async def get_user_roles(self, user: User) -> List[Role]:
        query = select(Role).join(UserRole, UserRole.role_id == Role.id)
        query = query.filter(UserRole.user_id == user.id)
        roles_db_obj = await self._session.execute(query)
        return [role for role, in roles_db_obj]

    async def add_user_role(self, user: User, role: Role) -> UserRole:
        user_role_db_obj = UserRole(user_id=user.id, role_id=role.id)
        self._session.add(user_role_db_obj)
        await self._session.commit()
        await self._session.refresh(user_role_db_obj)
        return user_role_db_obj

    async def delete_user_role(self, user: User, role: Role) -> UserRole | None:
        user_role_db_obj = await self._session.get(UserRole, [user.id, role.id])
        await self._session.delete(user_role_db_obj)
        await self._session.commit()
        return user_role_db_obj
