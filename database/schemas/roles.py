from pydantic import BaseModel


class RoleBase(BaseModel):
    role: str


class RoleFull(RoleBase):
    id: int
