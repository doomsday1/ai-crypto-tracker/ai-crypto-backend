from pydantic import BaseModel, ConfigDict
from typing import Optional


class UserBase(BaseModel):
    username: str
    full_name: str
    email: str
    disabled: bool


class UserOut(UserBase):
    id: int


# noinspection DuplicatedCode
class UserFull(UserOut):
    hashed_password: str


class UserCreate(UserBase):
    password: str


class UserUpdate(BaseModel):
    username: Optional[str] = None
    full_name: Optional[str] = None
    email: Optional[str] = None
    disabled: Optional[bool] = None
    password: Optional[str] = None

    model_config = ConfigDict(from_attributes=True)
