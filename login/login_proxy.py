import aiohttp
from fastapi import HTTPException
from starlette import status
from starlette.background import BackgroundTask
from starlette.requests import Request
from starlette.responses import StreamingResponse

import config


async def login_proxy(request: Request):
    host = config.server['login_service_host']
    port = config.server['login_service_port']

    path = request.url.path
    url = f"http://{host}:{port}{path}"
    params = request.url.query.encode("utf-8")
    headers = {str(k): str(v) for k, v in request.headers.items()}
    session = aiohttp.ClientSession()

    async def close():
        rp_resp.release()
        await session.close()

    try:
        rp_resp = await session.request(
            method=request.method,
            url=url,
            headers=headers,
            params=params,
            data=await request.body(),
            raise_for_status=True,
            timeout=aiohttp.ClientTimeout(total=None),
            ssl=False,
        )
        return StreamingResponse(
            rp_resp.content.iter_any(),
            status_code=rp_resp.status,
            headers=rp_resp.headers,
            background=BackgroundTask(close),
        )
    except aiohttp.ClientResponseError:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Proxy error",
        )
