import asyncio
from asyncio import Task

from fastapi import FastAPI, Header
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse, RedirectResponse

from database.api import db_router
from login.login_proxy import login_proxy

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*']
)

app.include_router(
    db_router,
    prefix="/db",
    tags=["Database"],
)

app.add_route("/login{path:path}", login_proxy, ["GET", "POST"])


@app.get("/")
async def index():
    return FileResponse("frontend/index.html", media_type="text/html; charset=utf-8")


app.mount("/", StaticFiles(directory="frontend"), name="static")

